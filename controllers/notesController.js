const Note = require('../models/Note');
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const fs = require("fs");

class notesController {
    async getNotes(req, res) {
        try {
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            const userId = decoded.id;
            const {offset = 0, limit = 0} = req.body;
            const notes = await Note.find({userId}).skip(offset).limit(limit);
            fs.appendFile('logs.log', `Code 200: Notes successfully received.\n`, () => {});
            return res.status(200).json(notes);
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {});
            return res.status(500).json("Server error");
        }
    }

    async createNote(req, res) {
        try {
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            const userId = decoded.id;
            const text = req.body.text;
            if (text === undefined) {
                fs.appendFile('logs.log', `Code 400: Missing the text field.\n`, () => {});
                return res.status(400).json({message: "Missing the text field"});
            }
            const note = await new Note({
                userId: `${userId}`,
                completed: false,
                text: `${text}`,
                createdDate: `${Date.now()}`
            })
            await note.save();
            fs.appendFile('logs.log', `Code 200: Note was successfully created.\n`, () => {});
            return res.status(200).json({message: "Note was successfully created"});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {});
            return res.status(500).json({message: "Server error"});
        }
    }

    async getNoteById(req, res) {
        try {
            const noteId = mongoose.Types.ObjectId(req.params.id);
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            const userId = decoded.id;
            const note = await Note.findOne({_id: noteId});
            if (note.userId !== userId) {
                fs.appendFile('logs.log', `Code 400: You can't access not your notes.\n`, () => {});
                return res.status(400).json({message: "You can't access not your notes"});
            }
            fs.appendFile('logs.log', `Code 200: Note was successfully received.\n`, () => {});
            return res.status(200).json(note);
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {});
            return res.status(500).json({message: "Server error"});
        }
    }

    async updateNote(req, res) {
        try {
            const {text} = req.body;
            const noteId = mongoose.Types.ObjectId(req.params.id);
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            const userId = decoded.id;
            const note = await Note.findOne({_id: noteId});
            if (note.userId !== userId) {
                fs.appendFile('logs.log', `Code 400: You can't edit not your notes.\n`, () => {});
                return res.status(400).json({message: "You can't edit not your notes"});
            }
            if (text === undefined) {
                fs.appendFile('logs.log', `Code 400: Specify the text field please.\n`, () => {});
                return res.status(400).json({message: "Specify the text field please"});
            }
            note.text = text;
            await note.save();
            fs.appendFile('logs.log', `Code 200: Note was successfully updated.\n`, () => {});
            return res.status(200).json(note);
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {});
            return res.status(500).json({message: "Server error"});
        }
    }

    async changeNoteState(req, res) {
        try {
            const noteId = mongoose.Types.ObjectId(req.params.id);
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            const userId = decoded.id;
            const note = await Note.findOne({_id: noteId});
            if (note.userId !== userId) {
                fs.appendFile('logs.log', `Code 400: You can't access not your notes.\n`, () => {});
                return res.status(400).json({message: "You can't access not your notes"});
            }

            switch (note.completed) {
                case false:
                    note.completed = true;
                    await note.save();
                    fs.appendFile('logs.log', `Code 200: Note checked.\n`, () => {});
                    return res.status(200).json(note);
                case true:
                    note.completed = false;
                    await note.save();
                    fs.appendFile('logs.log', `Code 200: Note checked.\n`, () => {});
                    return res.status(200).json(note);
            }
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {});
            return res.status(500).json({message: "Server error"});
        }
    }

    async deleteNote(req, res) {
        try {
            const noteId = mongoose.Types.ObjectId(req.params.id);
            const token = req.headers.authorization.split(' ')[1];
            const decoded = jwt.decode(token);
            const userId = decoded.id;
            const note = await Note.findOne({_id: noteId});
            if (note.userId !== userId) {
                fs.appendFile('logs.log', `Code 400: You can't access not your notes.\n`, () => {});
                return res.status(400).json({message: "You can't access not your notes"});
            }
            note.deleteOne();
            fs.appendFile('logs.log', `Code 200: Note was successfully deleted.\n`, () => {});
            return res.status(200).json({message: "Note was successfully deleted"});
        } catch (e) {
            console.log(e);
            fs.appendFile('logs.log', `Code 500: Server error.\n`, () => {});
            return res.status(500).json({message: "Server error"});
        }
    }
}

module.exports = new notesController();