const Router = require('express');
const router = new Router();
const controller = require('../controllers/userController');
const authMiddleware = require('../middlewares/authMiddleware');
const {check} = require("express-validator");

router.get("/me",authMiddleware, controller.getUser);
router.delete("/me",authMiddleware, controller.deleteUser);
router.patch("/me",[
    authMiddleware,
    check('oldPassword', "Password must be more than 4 and less than 16 symbols")
        .isLength({min: 4, max: 16}).notEmpty(),
    check('newPassword', "Password must be more than 4 and less than 16 symbols")
        .isLength({min: 4, max: 16}).notEmpty()
], controller.updateUser);


module.exports = router;