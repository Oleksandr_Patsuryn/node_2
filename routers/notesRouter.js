const Router = require('express');
const router = new Router();
const controller = require('../controllers/notesController');
const authMiddleware = require('../middlewares/authMiddleware');


router.get("/notes", authMiddleware, controller.getNotes);
router.post("/notes", authMiddleware, controller.createNote);
router.get("/notes/:id", authMiddleware, controller.getNoteById);
router.put("/notes/:id", authMiddleware, controller.updateNote);
router.patch("/notes/:id", authMiddleware, controller.changeNoteState);
router.delete("/notes/:id", authMiddleware, controller.deleteNote);


module.exports = router;