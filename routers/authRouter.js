const Router = require('express');
const router = new Router();
const controller = require('../controllers/authController');
const {check} = require('express-validator');

router.post('/register', [
    check('username', "Username can not be empty").notEmpty(),
    check('password', "Password must be more than 4 and less than 16 symbols")
        .isLength({min: 4, max: 16})
], controller.registration);
router.post('/login', controller.login);


module.exports = router;