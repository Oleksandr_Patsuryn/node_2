const {Schema, model} = require('mongoose');

const Note = new Schema({
    userId: {type: String, unique: false, required: true},
    completed: {type: Boolean, unique: false, required: true},
    text: {type: String, unique: false, required: false},
    createdDate: {type: Date, unique: false, required: true},
})

module.exports = model('Note', Note);