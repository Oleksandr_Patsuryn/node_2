const {Schema, model} = require('mongoose');

const User = new Schema({
    username: {type: String, ref: "Credentials.username"},
    createdDate: {type: Date, unique: false, required: true}
});

module.exports = model('User', User);